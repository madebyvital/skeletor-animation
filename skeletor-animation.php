<?php

namespace Vital;

/*
Plugin Name: Skeletor Block Settings — Animation
Description: Adds support for block intro animations
Version: 1.0.6
Requires PHP: 8
Requires at least: 6.1.0
Tested up to: 6.3.2
Author: Vital
Author URI: https://vitaldesign.com
Download URL: https://bitbucket.org/madebyvital/skeletor-animation/get/master.zip
Text Domain: skeletor-animation
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Last Updated: 2024-09-25 13:35:00
*/

// Exit if accessed directly
if (!defined('ABSPATH')) {
	exit;
}

/**
 * Enable Animation Settings for blocks.
 */
class SkeletorBlockAnimation {
	/**
	 * Called on `after_setup_theme`
	 *
	 * Bind actions here
	 *
	 * @return void
	 */
	public static function setup() {
		add_action('wp_enqueue_scripts', [__CLASS__, 'enqueue_frontend_assets']);
		add_action('enqueue_block_editor_assets', [__CLASS__, 'enqueue_block_editor_assets']);
	}

	public static function enqueue_frontend_assets() {
		$asset = include(__DIR__ . '/build/frontend.asset.php');
		if (!$asset) {
			return;
		}

		$plugin_dir_url = plugin_dir_url(__FILE__);

		wp_enqueue_script(
			'skeletor_animation',
			$plugin_dir_url . 'build/frontend.js',
			$asset['dependencies'],
			$asset['version'],
			true
		);

		wp_enqueue_style(
			'skeletor_animation',
			$plugin_dir_url . 'build/animation.scss.css',
			[],
			$asset['version']
		);
	}

	/**
	 * Called on `enqueue_block_editor_assets`
	 *
	 * Enqueue the plugin frontend script in the block editor
	 *
	 * @return void
	 */
	public static function enqueue_block_editor_assets() {
		$screen = get_current_screen();
		if (!$screen || !$screen->is_block_editor) {
			return;
		}

		$asset = include(__DIR__ . '/build/index.asset.php');
		$fasset = include(__DIR__ . '/build/frontend.asset.php');

		if (!$asset || !$fasset) {
			return;
		}

		$plugin_dir_url = plugin_dir_url(__FILE__);

		wp_enqueue_script(
			'skeletor_animation_backend',
			$plugin_dir_url . '/build/index.js',
			$asset['dependencies'],
			$asset['version'],
			true
		);

		wp_enqueue_style(
			'skeletor_animation',
			$plugin_dir_url . 'build/animation.scss.css',
			[],
			$fasset['version']
		);
	}
}

add_action('after_setup_theme', ['\Vital\SkeletorBlockAnimation', 'setup']);

define('SKELETOR_ANIMATION_VERSION', '1.0.6');

if (!class_exists('\Skeletor\Plugin_Updater')) {
	require_once(__DIR__ . '/class--plugin-updater.php');
}

$updater = new \Skeletor\Plugin_Updater(
	plugin_basename(__FILE__),
	SKELETOR_ANIMATION_VERSION,
	'https://bitbucket.org/madebyvital/skeletor-animation/raw/HEAD/package.json'
);
