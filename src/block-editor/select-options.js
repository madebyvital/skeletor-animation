const { __ } = wp.i18n;

export const animationOptions = [
	{ value: '',            label: __('None') },
	{ value: 'fade-in',     label: __('Fade In') },
	{ value: 'slide-up',    label: __('Slide Up') },
	{ value: 'slide-right', label: __('Slide Right') },
	{ value: 'slide-left',  label: __('Slide Left') },
];
